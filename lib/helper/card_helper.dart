import 'package:flutter_application_1/models/mons_atk_mod.dart';

class CardHelper {
   static List<MonsAtkMod> generateMonsAtkModObject() {
     List<MonsAtkMod> cards = [];
     const zeroCount = 6;
     const oneCount = 5;

     for(var i = 0; i < zeroCount; i++) {
       cards.add(MonsAtkMod(name: AtkModNames.add_0.name));
     }
     for(var i = 0; i < oneCount; i++) {
       cards.add(MonsAtkMod(name: AtkModNames.add_1.name));
     }
     for(var i = 0; i < oneCount; i++) {
       cards.add(MonsAtkMod(name: AtkModNames.minus_1.name));
     }

     cards.add(MonsAtkMod(name: AtkModNames.add_2.name));
     cards.add(MonsAtkMod(name: AtkModNames.minus_2.name));
     cards.add(MonsAtkMod(name: AtkModNames.mult_2.name, isReshuffle: true));
     cards.add(MonsAtkMod(name: AtkModNames.cancel.name, isReshuffle: true));

     return cards;
  }


}