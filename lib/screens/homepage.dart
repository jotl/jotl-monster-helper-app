import 'package:flutter/material.dart';
import 'package:flutter_application_1/functions/init_attack_mod.dart';
import 'package:flutter_application_1/models/monster_attack_mod_model.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  late List<AttackModifier> cardLists;
  List<AttackModifier> discardPile = [];
  int tapCount = 0;

  @override
  void initState() {
    super.initState();
    cardLists = generateList();
    cardLists.shuffle();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("TestHead"),
      ),
      body: InteractiveViewer(
        panEnabled: false,
        boundaryMargin: const EdgeInsets.all(10),
        minScale: 0.5,
        maxScale: 4,
        child: Row(children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Center(
                child: GestureDetector(
                  onTap: () => {
                    discardPile.add(cardLists[0]),
                    cardLists.remove(cardLists[0]),
                    tapCount++,
                    print(cardLists[0].imagePath),
                    print(discardPile.isEmpty == false
                        ? discardPile[0].imagePath
                        : "eh"),
                    setState(() {})
                  },
                  child: Image.asset(
                    'lib/assets/attack_mod_back.png',
                    scale: 4,
                  ),
                ),
              )
            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Center(
                child: Image.asset(
                  discardPile.isEmpty == true
                      ? 'lib/assets/attack_mod_back.png'
                      : discardPile[discardPile.length - 1].imagePath,
                  scale: 4,
                ),
              )
            ],
          )
        ]),
      ),
    );
  }
}
