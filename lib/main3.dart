import 'package:flutter/material.dart';

import 'cards/mons_atk_mod.dart';

void main() {
  runApp(const MyApp());
}
class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Gloomhaven helper',
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Gloomhaven'),
        ),
        bottomNavigationBar: BottomAppBar(
          child: Container(
            height: 80,
            alignment: Alignment.bottomLeft,
            margin: const EdgeInsets.all(10),
            child: Row(children: const [
              Text("next"),
              MonsterAtkMod(),
            ]),
          ),
        ),
        body: const Center(
          // child: Units(),
          child: RandomWords(),
        ),
      )
    );
  }
}

// units are list of characters and enemies
class Units extends StatefulWidget {
  const Units({Key? key}) : super(key: key);

  @override
  State<Units> createState() => _UnitsState();
}

class _UnitsState extends State<Units> {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}


class RandomWords extends StatefulWidget {
  const RandomWords({Key? key}) : super(key: key);

  @override
  State<RandomWords> createState() => _RandomWordsState();
}

class _RandomWordsState extends State<RandomWords> {
  // final _suggestions = <String>[];

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemBuilder: (context, i) {

          if(i.isOdd) return const Divider();
          //
          // final index = i ~/ 2;
          // if (index >= _suggestions.length) {
          //   _suggestions.add("monstername");
          // }

          return ListTile(
            title: Text('monster-$i'),
            trailing: const Icon(
              Icons.favorite_border
            ),
          );
        }
    );
    // return const Text("Monster name");
  }
}
