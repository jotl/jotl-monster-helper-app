import 'package:flutter/material.dart';
import 'package:flutter_application_1/helper/card_helper.dart';
import 'package:flutter_application_1/models/mons_atk_mod.dart';

class MonsterAtkMod extends StatefulWidget {
  const MonsterAtkMod({Key? key}) : super(key: key);

  @override
  State<MonsterAtkMod> createState() => _MonsterAtkModState();
}

class _MonsterAtkModState extends State<MonsterAtkMod> {
  List<MonsAtkMod> _drawPile = [];
  // Animation<double>? _frontRotation;

  @override
  void initState() {
    super.initState();
    _drawPile = CardHelper.generateMonsAtkModObject();
    _drawPile.shuffle();
  }
  @override
  Widget build(BuildContext context) {
    print(_drawPile.toString());
    // return AnimatedBuilder(animation: animation, builder: builder)
    return Image.asset("lib/assets/attack_mod_back.png");
  }
}
