import 'package:flutter_application_1/models/monster_attack_mod_model.dart';

List<AttackModifier> generateList() {
  List<AttackModifier> cards = [];
  for (var i = 0; i < 6; i++) {
    cards.add(AttackModifier(0, "add", "none", "lib/assets/add_0.png"));
  }
  for (var j = 0; j < 5; j++) {
    cards.add(AttackModifier(1, "sub", "none", "lib/assets/minus_1.png"));
  }
  for (var j = 0; j < 1; j++) {
    cards.add(AttackModifier(2, "sub", "none", "lib/assets/minus_2.png"));
  }
  for (var j = 0; j < 5; j++) {
    cards.add(AttackModifier(1, "add", "none", "lib/assets/add_1.png"));
  }
  for (var j = 0; j < 1; j++) {
    cards.add(AttackModifier(2, "add", "none", "lib/assets/add_2.png"));
  }
  for (var j = 0; j < 1; j++) {
    cards.add(AttackModifier(2, "mult", "shuffle", "lib/assets/mult_2.png"));
  }
  for (var j = 0; j < 1; j++) {
    cards.add(AttackModifier(0, "null", "shuffle", "lib/assets/null.png"));
  }
  print("cards ${cards}");
  return cards;
}
