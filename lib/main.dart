import 'package:flutter/material.dart';
import 'helper/card_helper.dart';
import 'models/mons_atk_mod.dart';

void main() {
  runApp(const MaterialApp(home: MyApp()));
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  List<MonsAtkMod> _drawPile = [];
  final List<MonsAtkMod> _discardPile = [];
  MonsAtkMod _drawnCard = MonsAtkMod(name: "attack_mod_back");

  @override
  void initState() {
    super.initState();
    _drawPile = CardHelper.generateMonsAtkModObject();
    _drawPile.shuffle();
  }

  @override
  Widget build(BuildContext context) {
    print(_drawPile);
    return SafeArea(
      child: Scaffold(
        body: Center(
          child: Row(
            children: [
              TextButton(
                onPressed: () {
                  nextRound();
                },
                child: Icon(Icons.navigate_next)
              ),
              Expanded(
                child: TextButton(
                  onPressed: () {
                    print('on click draw pile');
                    drawAtkModCard();
                  },
                  child: Image.asset('lib/assets/attack_mod_back.png')
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Stack(
                    alignment: Alignment.center,
                    children: [
                      Image.asset('lib/assets/${_drawnCard.getName()}.png'),
                      if (_discardPile.isEmpty)
                        Container(
                          color: Colors.white,
                          child: Text("Discard Pile")
                        ),
                    ],
                  ),
                ))
            ],
          ),
        ),
      ),
    );
  }

  void drawAtkModCard() {
    setState(() {
      if(_drawPile.isNotEmpty) {
        _drawnCard = _drawPile.removeLast();
        _discardPile.add(_drawnCard);
      }
    });
  }

  void nextRound() {
    print("next round");
    setState(() {
      if(_drawnCard.isReshuffle || _drawPile.isEmpty) {
        _drawPile.addAll(_discardPile);
        _discardPile.clear();
        _drawnCard = MonsAtkMod(name: "attack_mod_back");
      }
    });
  }
}
