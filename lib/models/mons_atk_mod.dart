enum AtkModNames {
  add_0, add_1, add_2, minus_1,
  minus_2, mult_2, cancel
}

class MonsAtkMod {
  final String name;
  final bool isReshuffle;

  MonsAtkMod({this.name="", this.isReshuffle=false});

  String getName() {
    return name;
  }
  @override
  String toString() {
    return '$name-$isReshuffle';
  }
}
