class AttackModifier {
  final int modifierValue;
  final String operation;
  final String effect;
  final String imagePath;

  AttackModifier(
      this.modifierValue, this.operation, this.effect, this.imagePath);
}
